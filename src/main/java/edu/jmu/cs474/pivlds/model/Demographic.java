package edu.jmu.cs474.pivlds.model;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import edu.jmu.cs474.pivlds.PivldsDb;

/**
 * The model class for the pivlds database table Demographic.
 * 
 * @author James Arlow
 *
 */
public class Demographic implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Disabil disabil = Disabil.ALL;
	Disadva disadva = Disadva.ALL;
	Gender gender = Gender.ALL;
	LEP lep = LEP.ALL;
	Race race = Race.ALL;

	public static enum Disabil implements Serializable {
		ALL, Y, N;
	}

	public static enum Disadva implements Serializable {
		ALL, Y, N;
	}

	public static enum Gender implements Serializable {
		ALL, M, F;
	}

	public static enum LEP implements Serializable {
		ALL, Y, N;
	}

	public static enum Race implements Serializable {
		ALL, _0, _1, _2, _3, _4, _99;

		public String sqlString() {
			if (this == ALL)
				return this.toString();
			return this.toString().substring(1);
		}
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Disabil getDisabil() {
		return disabil;
	}

	public void setDisabil(Disabil disabil) {
		this.disabil = disabil;
	}

	public LEP getLep() {
		return lep;
	}

	public void setLep(LEP lep) {
		this.lep = lep;
	}

	public Disadva getDisadva() {
		return disadva;
	}

	public void setDisadva(Disadva disadva) {
		this.disadva = disadva;
	}

	public Race getRace() {
		return race;
	}

	public void setRace(Race race) {
		this.race = race != null ? race : Race.ALL;
	}

	final static String key_query = "SELECT dem_pk FROM Demographic WHERE " //
			+ " race ilike ? "// 1
			+ "AND gender ilike ? " // 2
			+ "AND disabil ilike ? " // 3
			+ "AND disadva ilike ? " // 4
			+ "AND lep ilike ?" // 5
	;

	public Integer getKey() {
		try (Connection c = PivldsDb.getConnection(); PreparedStatement ps = c.prepareStatement(key_query)) {
			ps.setString(1, race.sqlString());
			ps.setString(2, gender.toString());
			ps.setString(3, disabil.toString());
			ps.setString(4, disadva.toString());
			ps.setString(5, lep.toString());

			try (ResultSet rs = ps.executeQuery();) {
				rs.next();
				return rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public String toString() {
		boolean filtered = false;
		StringBuilder sb = new StringBuilder();

		if (race != Race.ALL) {
			filtered = true;
			sb.append("Race(");
			sb.append(race.sqlString());
			sb.append(")");
		}
		if (gender != Gender.ALL) {
			if (filtered)
				sb.append(", ");
			filtered = true;
			sb.append("Gender(");
			sb.append(gender);
			sb.append(")");
		}
		if (disabil != Disabil.ALL) {
			if (filtered)
				sb.append(", ");
			filtered = true;
			sb.append("Disabil(");
			sb.append(disabil);
			sb.append(")");
		}
		if (disadva != Disadva.ALL) {
			if (filtered)
				sb.append(", ");
			filtered = true;
			sb.append("Disadva(");
			sb.append(disadva);
			sb.append(")");
		}

		if (lep != LEP.ALL) {
			if (filtered)
				sb.append(", ");
			filtered = true;
			sb.append("LEP(");
			sb.append(lep);
			sb.append(")");
		}

		if (!filtered) {
			sb.append("ALL");
		}

		return sb.toString();
	}

	public void reset() {
		setRace(Race.ALL);
		setGender(Gender.ALL);
		setDisabil(Disabil.ALL);
		setDisadva(Disadva.ALL);
		setLep(LEP.ALL);
	}

}
