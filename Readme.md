#piVLDS GP5 - Working Prototype

## Assignment Goals

See [instructor reference](https://w3.cs.jmu.edu/mayfiecs/cs474/project/gp5.html).

## Target Environment

### Server Libraries

* Tomcat7 
* JSF 2.2 
* CDI 1.2

### Database Config

The piVLDS postgress database must be accessible on one of the following host:ports:

* `db.cs.jmu.edu:5432`
* `localhost:5432`
* `localhost:6432`

Use [git@bitbucket.org:pivlds/remote-bridge.git](https://bitbucket.org/pivlds/remote-bridge) to map from offsite through
student server.

File `~/pivldsauth` must be a property file containing `dbuser` and `dbpass` entries.  If the file does not exist, the connection manager will prompt via JOptionPane Dialogs from the local system.

### IDE Format

** Eclipse : javaee-mars **

To resolve errors after initial import:

* In Eclipse:
* Project Explorer > Right-click project node
* Maven > Update Project 

## References

Derived from template project: [git@bitbucket.org:pivlds/jsf-template.git](https://bitbucket.org/pivlds/jsf-template)