package edu.jmu.cs474.pivlds.jsf;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import edu.jmu.cs474.pivlds.beans.TestSelector;
import edu.jmu.cs474.pivlds.model.TestId;

/**
 * {@link FacesConverter} for {@link TestId}. Allows the MVC framework to pass
 * references for content dynamically bound to database.
 * <p>
 * The canonical string representation is the primary key as a string.
 * 
 * @author James
 *
 */
@FacesConverter(forClass = TestId.class)
public class TestConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		return TestSelector.find(Integer.parseInt(arg2));
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		TestId test = (TestId) arg2;
		return "" + test.getTest_pk();
	}

}
