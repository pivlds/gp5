package edu.jmu.cs474.pivlds.reports;

import java.util.Properties;

import javax.inject.Inject;

import edu.jmu.cs474.pivlds.beans.DemographicSelector;
import edu.jmu.cs474.pivlds.beans.SchoolSelector;
import edu.jmu.cs474.pivlds.beans.TestSelector;
import edu.jmu.cs474.pivlds.jsf.PageMessage;

/**
 * Common superclass for reports that target a specific test.
 * 
 * @author James Arlow
 *
 */
public class TestReport {

	@Inject
	SchoolSelector schSelect;

	@Inject
	DemographicSelector demSelect;

	@Inject
	TestSelector testSelect;

	Boolean ready;
	boolean dirty = false;

	Properties context = new Properties();

	public void readContext() {
		String pre = "" + context;

		Properties props = loadContext();
		if (checkDirty(props)) {
			dirty = true;
			context = props;
		}

		System.out.println(
				"dirty?: [" + dirty + "] " + getClass().getSimpleName() + (dirty ? " | " + pre + " => " + props : ""));
	}

	public Properties loadContext() {
		Properties props = new Properties();

		props.setProperty("sch", "" + schSelect.getCurrent());
		props.setProperty("dem", "" + demSelect.getCurrent());
		props.setProperty("test", "" + testSelect.getSelected());

		return props;
	}

	public boolean checkReady() {
		if (ready != null)
			return ready;

		if (schSelect.getCurrent() == null || !schSelect.getCurrent().isValid()) {
			PageMessage.message("Invalid School ");
			ready = false;
		} else
			ready = true;
		return ready;
	}

	public boolean checkDirty(Properties props) {
		if (!context.equals(props))
			return true;

		for (Object key : context.keySet()) {
			if (!context.get(key).equals(props.get(key)))
				return true;
		}

		return false;
	}

}
