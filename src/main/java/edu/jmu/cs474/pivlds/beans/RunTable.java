package edu.jmu.cs474.pivlds.beans;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * Request JNDI bean that can be used to prevent slow queries from loading on
 * page start.
 * <p>
 * Not needed since database indexes were implemented.
 * 
 * @author James Arlow
 *
 */
@Named
@RequestScoped
public class RunTable implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	boolean show = false;

	public void toggleShow() {
		show = !show;
	}

	public boolean getShow() {
		return show;
	}

}
