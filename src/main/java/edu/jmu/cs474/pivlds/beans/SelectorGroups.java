package edu.jmu.cs474.pivlds.beans;

import javax.inject.Named;
import javax.inject.Singleton;

import edu.jmu.cs474.pivlds.model.TestAchievementGroup;

/**
 * Provides enum values for option-boxes/lists.
 * <p>
 * TODO: Migrate other enum option lists here.
 * 
 * @author James Arlow
 *
 */
@Named
@Singleton
public class SelectorGroups {

	public TestAchievementGroup[] getTags() {
		return TestAchievementGroup.values();
	}

}
