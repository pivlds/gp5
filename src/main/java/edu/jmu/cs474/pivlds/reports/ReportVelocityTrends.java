package edu.jmu.cs474.pivlds.reports;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import edu.jmu.cs474.pivlds.PivldsDb;
import edu.jmu.cs474.pivlds.jsf.PageMessage;

/**
 * View level bean for calling `report_velocity_trends(...)`.
 * 
 * @author James Arlow
 *
 */
@Named
@ViewScoped
public class ReportVelocityTrends extends TestReport implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Data model for the postgres type returned by the report function.
	 * 
	 * @author James Arlow
	 *
	 */
	public static class Row implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		int year;
		float upper;
		float lower;
		float upper_weighted;
		float lower_weighted;

		public void read(ResultSet rs) throws SQLException {
			year = rs.getInt("year");
			upper = rs.getFloat("upper");
			lower = rs.getFloat("lower");
			upper_weighted = rs.getFloat("upper_weighted");
			lower_weighted = rs.getFloat("lower_weighted");
		}

		public int getYear() {
			return year;
		}

		public float getUpper() {
			return upper;
		}

		public float getLower() {
			return lower;
		}

		public float getUpper_weighted() {
			return upper_weighted;
		}

		public float getLower_weighted() {
			return lower_weighted;
		}

	}

	List<Row> results = new LinkedList<>();

	public void reset(Exception e) {
		results.clear();
		if (e != null)
			PageMessage.message(e);
	}

	public List<Row> getResults() {
		if (checkReady()) {
			runQuery();
		}
		return results;
	}

	public void runQuery() {
		readContext();
		if (!dirty)
			return;
		System.out.println("query: " + getClass().getSimpleName() + " | " + context);

		results.clear();
		try {
			try (Connection c = PivldsDb.getConnection();
					PreparedStatement ps = c.prepareStatement(
							"SELECT * FROM report_velocity_trends(test_id(?), school(?), demographic(?));");) {
				ps.setInt(1, testSelect.getSelected().getTest_pk());
				ps.setInt(2, schSelect.getCurrent().getSch_pk());
				ps.setInt(3, demSelect.getCurrent().getKey());
				try (ResultSet rs = ps.executeQuery();) {
					if (!rs.next()) {
						dirty = true;
						throw new Exception("No results");
					} else {
						do {
							Row s = new Row();
							s.read(rs);
							results.add(s);
						} while (rs.next());
						dirty = false;
					}
				}
			}
		} catch (Exception e) {
			reset(e);
		}
	}

}
