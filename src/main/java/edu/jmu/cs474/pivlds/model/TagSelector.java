package edu.jmu.cs474.pivlds.model;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 * Session level bean for selecting {@link TestAchievementGroup}.
 * @author James Arlow
 */
@Named
@SessionScoped
public class TagSelector implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	TestAchievementGroup selected;

	public void setSelected(TestAchievementGroup selected) {
		System.out.println("set: " + selected);
		this.selected = selected;
	}

	public TestAchievementGroup getSelected() {
		return selected;
	}

	public boolean isValid() {
		return selected != null;
	}

	public TestAchievementGroup[] getValues() {
		return TestAchievementGroup.values();
	}
	
	@PostConstruct
	public void setDefault() {
		selected = TestAchievementGroup.MID;
	}
}
