package edu.jmu.cs474.pivlds.tests;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 * A simple test for the JSF messages system.
 * 
 * @author James Arlow
 *
 */
@Named
@RequestScoped
public class TestFacesMessage {

	public void init() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		FacesMessage facesMessage = new FacesMessage("This is a test message");
		facesContext.addMessage(null, facesMessage);
	}
}
