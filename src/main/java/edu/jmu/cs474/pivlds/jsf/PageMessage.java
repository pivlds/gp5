package edu.jmu.cs474.pivlds.jsf;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * Utility class for passing messages to the h:message jsf component.
 * 
 * @author James Arlow
 *
 */
public class PageMessage {

	public static void message(String s) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(s));
	}

	public static void message(Exception e) {
		message(e.toString());
		e.printStackTrace();
	}

}
