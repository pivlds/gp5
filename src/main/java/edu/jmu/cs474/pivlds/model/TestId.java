package edu.jmu.cs474.pivlds.model;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Data model for `test_id`.
 * 
 * @author James Arlow
 *
 */
public class TestId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	Integer tlvl_pk;
	Integer tcat_pk;
	Integer test_pk;

	String subject;
	String section;
	String tlvl_code;

	public void read(ResultSet rs) throws SQLException {
		tlvl_pk = rs.getInt("tlvl_pk");
		tcat_pk = rs.getInt("tcat_pk");
		test_pk = rs.getInt("test_pk");

		subject = rs.getString("subject");
		section = rs.getString("section");
		tlvl_code = rs.getString("tlvl_code");
	}

	public Integer getTlvl_pk() {
		return tlvl_pk;
	}

	public Integer getTcat_pk() {
		return tcat_pk;
	}

	public Integer getTest_pk() {
		return test_pk;
	}

	public String getSubject() {
		return subject;
	}

	public String getSection() {
		return section;
	}

	public String getTlvl_code() {
		return tlvl_code;
	}

	public String toString() {
		return subject + " :: " + section + " [" + tlvl_code + "]";  
	}
	
}
