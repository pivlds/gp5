package edu.jmu.cs474.pivlds.reports;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import edu.jmu.cs474.pivlds.PivldsDb;
import edu.jmu.cs474.pivlds.jsf.PageMessage;
import edu.jmu.cs474.pivlds.model.TagSelector;

/**
 * View level bean for calling `report_test_history(...)`.
 * 
 * @author James Arlow
 *
 */
@Named
@ViewScoped
public class ReportTestHistory extends TestReport implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Data model for the postgres type returned by the report function.
	 * 
	 * @author James Arlow
	 *
	 */
	public static class Row implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		int year;
		float tag_rate;
		float tag_diff;
		float dem_weight;
		float rate_weighted;
		float diff_weighted;
		float velocity;
		float velocity_weighted;

		public void read(ResultSet rs) throws SQLException {
			year = rs.getInt("year");
			tag_rate = rs.getFloat("tag_rate");
			tag_diff = rs.getFloat("tag_diff");
			dem_weight = rs.getFloat("dem_weight");
			rate_weighted = rs.getFloat("rate_weighted");
			diff_weighted = rs.getFloat("diff_weighted");
			velocity = rs.getFloat("velocity");
			velocity_weighted = rs.getFloat("velocity_weighted");
		}

		public int getYear() {
			return year;
		}

		public float getTag_rate() {
			return tag_rate;
		}

		public float getTag_diff() {
			return tag_diff;
		}

		public float getDem_weight() {
			return dem_weight;
		}

		public float getRate_weighted() {
			return rate_weighted;
		}

		public float getDiff_weighted() {
			return diff_weighted;
		}

		public float getVelocity() {
			return velocity;
		}

		public float getVelocity_weighted() {
			return velocity_weighted;
		}

	}

	List<Row> results = new LinkedList<>();

	@Inject
	TagSelector tagSelect;

	public void runQuery() {
		readContext();
		if (!dirty)
			return;
		System.out.println("query: " + getClass().getSimpleName() + " | " + context);
		results.clear();
		try {
			try (Connection c = PivldsDb.getConnection();
					PreparedStatement ps = c.prepareStatement(
							"SELECT * FROM report_test_history(tag(?), test_id(?), school(?), demographic(?));");) {
				ps.setInt(1, tagSelect.getSelected().tag_pk);
				ps.setInt(2, testSelect.getSelected().getTest_pk());
				ps.setInt(3, schSelect.getCurrent().getSch_pk());
				ps.setInt(4, demSelect.getCurrent().getKey());
				try (ResultSet rs = ps.executeQuery();) {
					if (!rs.next()) {
						dirty = true;
						throw new Exception("No results");
					} else {
						do {
							Row s = new Row();
							s.read(rs);
							results.add(s);
						} while (rs.next());
						dirty = false;
					}
				}
			}
		} catch (Exception e) {
			reset(e);
		}
	}

	public void reset(Exception e) {
		results.clear();
		if (e != null)
			PageMessage.message(e);
	}

	public List<Row> getResults() {
		System.out.println(FacesContext.getCurrentInstance().getCurrentPhaseId());
		if (checkReady()) {
			runQuery();
		}
		return results;
	}

	@Override
	public Properties loadContext() {
		Properties p = super.loadContext();
		p.put("tag", tagSelect.getSelected());
		return p;
	}

}
